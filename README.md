# kata bank

Kata implementing deposit and withdrawal operations on a bank account as well as the generation of an account statement
through a REST API

## Requirements

For building and running the application you need:

- [JDK 17](https://www.oracle.com/java/technologies/downloads/)
- [Maven 3](https://maven.apache.org)

- Run the application:

```shell
mvn spring-boot:run
```

Run the application tests:

```shell
mvn test
```
