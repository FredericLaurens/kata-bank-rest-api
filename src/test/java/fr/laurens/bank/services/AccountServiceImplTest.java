package fr.laurens.bank.services;

import fr.laurens.bank.domain.Account;
import fr.laurens.bank.domain.AccountOperationLog;
import fr.laurens.bank.domain.Clock;
import fr.laurens.bank.domain.OperationType;
import fr.laurens.bank.domain.services.StatementPrinter;
import fr.laurens.bank.persistence.AccountRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static fr.laurens.bank.domain.OperationType.DEPOSIT;
import static fr.laurens.bank.domain.OperationType.WITHDRAWAL;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AccountServiceImplTest {

    Account account;

    @Mock
    Clock clock;
    @Autowired
    StatementPrinter statementPrinter;
    @Mock
    private AccountRepository accountRepository;
    @Autowired
    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        account = new Account("ID1", clock);
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(500));
        when(accountRepository.findById("ID1")).thenReturn(Optional.of(account));
        when(clock.now()).thenReturn(LocalDate.of(2000, 10, 10));
    }

    @Test
    public void should_deposit_onto_account() {
        accountService.deposit("ID1", BigDecimal.valueOf(50.5));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(1);
        Assertions.assertThat(account.getAccountOperationLog().getAccountOperations())
                .extracting("operationType", "amount", "date", "resultingBalance")
                .isEqualTo(List.of(tuple(DEPOSIT, BigDecimal.valueOf(50.5), LocalDate.of(2000, 10, 10), BigDecimal.valueOf(50.5))));
    }

    @Test
    public void should_withdraw_from_account() {
        accountService.withdraw("ID1", BigDecimal.valueOf(50.5));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(1);
        Assertions.assertThat(account.getAccountOperationLog().getAccountOperations())
                .extracting("operationType", "amount", "date", "resultingBalance")
                .isEqualTo(List.of(tuple(WITHDRAWAL, BigDecimal.valueOf(50.5), LocalDate.of(2000, 10, 10), BigDecimal.valueOf(-50.5))));
    }

    @Test
    public void should_show_operation_log() {
        account.deposit(BigDecimal.valueOf(200));
        account.withdraw(BigDecimal.valueOf(50));
        AccountOperationLog accountOperationLog = accountService.generateAccountStatement("ID1");
        assertThat(accountOperationLog.getAccountOperations().size()).isEqualTo(2);
        Assertions.assertThat(accountOperationLog.getAccountOperations())
                .extracting("operationType", "amount", "date", "resultingBalance")
                .isEqualTo(List.of(
                        tuple(DEPOSIT, BigDecimal.valueOf(200), LocalDate.of(2000, 10, 10), BigDecimal.valueOf(200)),
                        tuple(OperationType.WITHDRAWAL, BigDecimal.valueOf(50), LocalDate.of(2000, 10, 10), BigDecimal.valueOf(150))));
    }

    @Test
    public void should_print_operation_log() {
        account.deposit(BigDecimal.valueOf(200));
        account.withdraw(BigDecimal.valueOf(50));
        String statement = accountService.printAccountStatement("ID1");
        assertThat(statement).isEqualTo("""
                | Operation  | Date       | Amount | Balance |
                | withdrawal | 10/10/2000 | 50,00  | 150,00  |
                | deposit    | 10/10/2000 | 200,00 | 200,00  |
                """);

    }
}
