package fr.laurens.bank.domain;


import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ClockTest {

    @Test
    public void should_be_a_singleton() {
        Clock clock = Clock.getInstance();
        Clock sameClock = Clock.getInstance();
        assertThat(sameClock).isSameAs(clock);
    }

    @Test
    public void should_return_today_s_date() {
        Clock clock = Clock.getInstance();
        assertThat(clock.now()).isEqualTo(LocalDate.now());
    }
}
