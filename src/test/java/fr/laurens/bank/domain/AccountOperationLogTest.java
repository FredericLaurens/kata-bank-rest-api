package fr.laurens.bank.domain;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AccountOperationLogTest {

    @Test
    public void should_create_an_empty_operation_log_for_an_account() {

        //when
        AccountOperationLog accountOperationLog = new AccountOperationLog();

        //then
        assertThat(accountOperationLog.getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    public void should_add_an_operation_to_the_operation_log() {

        //given
        AccountOperationLog accountOperationLog = new AccountOperationLog();
        AccountOperation operation = new AccountOperation(OperationType.DEPOSIT, BigDecimal.valueOf(100), LocalDate.of(2020, 2, 1), BigDecimal.valueOf(200));

        //when
        accountOperationLog.addAccountOperation(operation);

        //then
        assertThat(accountOperationLog.getAccountOperations().size()).isEqualTo(1);
        assertThat(accountOperationLog.getAccountOperations().get(0)).isEqualTo(operation);
    }


    @Test
    public void should_add_operations_to_the_operation_log_in_chronological_order() {

        //given
        AccountOperationLog accountOperationLog = new AccountOperationLog();
        AccountOperation firstOperation = new AccountOperation(OperationType.DEPOSIT, BigDecimal.valueOf(100), LocalDate.of(2020, 2, 1), BigDecimal.valueOf(200));
        AccountOperation secondOperation = new AccountOperation(OperationType.DEPOSIT, BigDecimal.valueOf(200), LocalDate.of(2020, 2, 1), BigDecimal.valueOf(400));

        //when
        accountOperationLog.addAccountOperation(firstOperation);
        accountOperationLog.addAccountOperation(secondOperation);

        //then
        assertThat(accountOperationLog.getAccountOperations().size()).isEqualTo(2);
        assertThat(accountOperationLog.getAccountOperations().get(0)).isEqualTo(firstOperation);
        assertThat(accountOperationLog.getAccountOperations().get(1)).isEqualTo(secondOperation);
    }

}