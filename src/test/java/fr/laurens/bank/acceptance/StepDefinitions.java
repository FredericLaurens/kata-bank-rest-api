package fr.laurens.bank.acceptance;

import fr.laurens.bank.domain.Account;
import fr.laurens.bank.domain.AccountOperationLog;
import fr.laurens.bank.domain.Clock;
import fr.laurens.bank.domain.services.StatementPrinter;
import fr.laurens.bank.persistence.AccountRepository;
import fr.laurens.bank.services.AccountService;
import fr.laurens.bank.services.AccountServiceImpl;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@CucumberContextConfiguration
@SpringBootTest
public class StepDefinitions {

    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final DateTimeFormatter DATE_FORMATTER;

    static {
        DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    }

    private AccountOperationLog accountOperationLog;

    @Mock
    Clock clock;

    @Autowired
    StatementPrinter statementPrinter;

    @Mock
    private AccountRepository accountRepository;

    @Autowired
    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    private String accountStatement;

    @ParameterType(".*")
    public LocalDate date(String date) {
        return LocalDate.parse(date, DATE_FORMATTER);
    }

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
        Account account = new Account("ID1", clock);
        when(accountRepository.findById("ID1")).thenReturn(Optional.of(account));
    }

    @Given("(a client makes )a {word} of {int} on {date}")
    public void clientMakesAnOperation(String operationType, int amount, LocalDate date) {

        when(clock.now()).thenReturn(date);

        if ("deposit".equals(operationType)) {
            accountService.deposit("ID1", BigDecimal.valueOf(amount));
        } else if ("withdrawal".equals(operationType)) {
            accountService.withdraw("ID1", BigDecimal.valueOf(amount));
        }
    }

    @When("he prints his account statement")
    public void clientPrintsHisAccountStatement() {

        accountStatement = accountService.printAccountStatement("ID1");
    }

    @Then("he would see:")
    public void printedAccountStatementShouldBe(DataTable dataTable) {

        assertThat(accountStatement).isEqualTo(dataTable.toString());
    }
}
