package fr.laurens.bank.printer;

import fr.laurens.bank.domain.Account;
import fr.laurens.bank.domain.Clock;
import fr.laurens.bank.domain.services.StatementPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BasicStatementPrinterTest {

    @Mock
    Clock clock = mock(Clock.class);

    private final Account account = new Account("ID", clock);

    private StatementPrinter statementPrinter;

    @BeforeEach
    public void getBasicStatementPrinterInstance() {
        statementPrinter = BasicStatementPrinter.getInstance();
    }

    @Test
    public void should_print_an_empty_account_statement() {

        //then
        assertThat(statementPrinter.printStatement(account.getAccountOperationLog()))
                .isEqualTo("| Operation | Date | Amount | Balance |");
    }

    @Test
    public void should_print_an_account_statement_with_operations() {

        //given
        when(clock.now()).thenReturn(LocalDate.of(2020, 1, 1));

        //when
        account.deposit(BigDecimal.valueOf(100));
        account.withdraw(BigDecimal.valueOf(50));

        //then
        assertThat(statementPrinter.printStatement(account.getAccountOperationLog())).isEqualTo("""
                | Operation  | Date       | Amount | Balance |
                | withdrawal | 01/01/2020 | 50,00  | 50,00   |
                | deposit    | 01/01/2020 | 100,00 | 100,00  |
                """);

    }
}