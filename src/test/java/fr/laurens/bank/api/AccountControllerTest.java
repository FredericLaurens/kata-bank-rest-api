package fr.laurens.bank.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.laurens.bank.api.request.AccountOperationRequest;
import fr.laurens.bank.domain.Account;
import fr.laurens.bank.domain.Clock;
import fr.laurens.bank.domain.services.StatementPrinter;
import fr.laurens.bank.persistence.AccountRepository;
import fr.laurens.bank.services.AccountService;
import fr.laurens.bank.services.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    private static final ObjectMapper objectMapper;
    private static final ObjectWriter objectWriter;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
    }

    @Mock
    Clock clock;
    private Account account;
    @Mock
    private AccountRepository accountRepository;
    @Autowired
    private StatementPrinter statementPrinter;
    @Autowired
    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();
    @Autowired
    @InjectMocks
    private AccountController accountController;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
        account = new Account("ID1", clock);
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(500));
        when(accountRepository.findById("ID1")).thenReturn(Optional.of(account));
        when(clock.now()).thenReturn(LocalDate.of(2000, 10, 10));
    }

    @Test
    public void should_deposit_onto_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID1", 100.50));
        this.mockMvc.perform(post("/accounts/deposit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    public void should_not_deposit_negative_amount_onto_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID1", -100.50));
        this.mockMvc.perform(post("/accounts/deposit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_not_deposit_onto_non_existing_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID2", 100.50));
        this.mockMvc.perform(post("/accounts/deposit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_withdraw_from_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID1", 100.50));
        this.mockMvc.perform(post("/accounts/withdraw")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk());
    }

    @Test
    public void should_not_withdraw_more_than_max_overdraft_from_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID1", 10000.50));
        this.mockMvc.perform(post("/accounts/withdraw")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void should_not_withdraw_from_non_existing_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID2", 10000.50));
        this.mockMvc.perform(post("/accounts/withdraw")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_not_withdraw_negative_amount_from_account() throws Exception {
        String requestJson = objectWriter.writeValueAsString(new AccountOperationRequest("ID2", -50.50));
        this.mockMvc.perform(post("/accounts/withdraw")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_show_operation_log() throws Exception {
        account.deposit(BigDecimal.valueOf(100));
        this.mockMvc.perform(get("/account/ID1/generate-statement"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"statement\":{\"accountOperations\":[{\"operationType\":\"DEPOSIT\",\"amount\":100,\"date\":[2000,10,10],\"resultingBalance\":100}]}}"));
    }

    @Test
    public void should_not_show_operation_log_for_non_existing_account() throws Exception {
        this.mockMvc.perform(get("/account/ID2/generate-statement"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void should_print_operation_log() throws Exception {
        account.deposit(BigDecimal.valueOf(100));
        this.mockMvc.perform(get("/account/ID1/print-statement"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"printedDocument\":\"| Operation | Date       | Amount | Balance |\\n| deposit   | 10/10/2000 | 100,00 | 100,00  |\\n\"}"));
    }

    @Test
    public void should_not_print_operation_log_for_not_existing_account() throws Exception {
        this.mockMvc.perform(get("/account/ID2/print-statement"))
                .andExpect(status().isBadRequest());
    }
}
