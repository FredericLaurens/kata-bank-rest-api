package fr.laurens.bank.api.request;

public record AccountOperationRequest(String accountNumber, double amount) {
}
