package fr.laurens.bank.api;

import fr.laurens.bank.api.request.AccountOperationRequest;
import fr.laurens.bank.api.response.AccountOperationErrorResponse;
import fr.laurens.bank.api.response.AccountOperationResponse;
import fr.laurens.bank.api.response.AccountStatementPrintResponse;
import fr.laurens.bank.api.response.AccountStatementResponse;
import fr.laurens.bank.domain.AllowedOverdraftExceededException;
import fr.laurens.bank.services.AccountService;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

@RestController
public class AccountController {

    @Autowired
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("accounts/deposit")
    public ResponseEntity<AccountOperationResponse> deposit(@RequestBody AccountOperationRequest accountOperationRequest) {
        try {
            BigDecimal amount = BigDecimal.valueOf(accountOperationRequest.amount());
            accountService.deposit(accountOperationRequest.accountNumber(), amount);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(AccountOperationErrorResponse.of(exception.getMessage()), HttpStatus.valueOf(Response.SC_BAD_REQUEST));
        }
        return new ResponseEntity<>(new AccountOperationResponse(), HttpStatus.valueOf(Response.SC_OK));
    }

    @PostMapping("accounts/withdraw")
    public ResponseEntity<AccountOperationResponse> withdraw(@RequestBody AccountOperationRequest accountOperationRequest) {
        try {
            BigDecimal amount = BigDecimal.valueOf(accountOperationRequest.amount());
            accountService.withdraw(accountOperationRequest.accountNumber(), amount);
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(AccountOperationErrorResponse.of(exception.getMessage()), HttpStatus.valueOf(SC_BAD_REQUEST));
        } catch (AllowedOverdraftExceededException exception) {
            return new ResponseEntity<>(AccountOperationErrorResponse.of(exception.getMessage()), HttpStatus.valueOf(Response.SC_FORBIDDEN));
        }
        return new ResponseEntity<>(HttpStatus.valueOf(Response.SC_OK));
    }

    @GetMapping("account/{accountNumber}/generate-statement")
    public ResponseEntity<AccountOperationResponse> generateAccountStatement(@PathVariable String accountNumber) {
        try {
            return new ResponseEntity<>(AccountStatementResponse.of(accountService.generateAccountStatement(accountNumber)), HttpStatus.valueOf(Response.SC_OK));
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(AccountOperationErrorResponse.of(exception.getMessage()), HttpStatus.valueOf(SC_BAD_REQUEST));
        }
    }

    @GetMapping("account/{accountNumber}/print-statement")
    public ResponseEntity<AccountOperationResponse> printAccountStatement(@PathVariable String accountNumber) {
        try {
            return new ResponseEntity<>(AccountStatementPrintResponse.of(accountService.printAccountStatement(accountNumber)), HttpStatus.valueOf(Response.SC_OK));
        } catch (IllegalArgumentException exception) {
            return new ResponseEntity<>(AccountOperationErrorResponse.of(exception.getMessage()), HttpStatus.valueOf(SC_BAD_REQUEST));
        }
    }
}
