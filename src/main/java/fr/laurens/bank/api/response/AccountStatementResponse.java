package fr.laurens.bank.api.response;

import fr.laurens.bank.domain.AccountOperationLog;
import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class AccountStatementResponse extends AccountOperationResponse {

    public AccountOperationLog statement;

    public AccountStatementResponse(AccountOperationLog statement) {
        super();
        this.statement = statement;
    }

    public static AccountStatementResponse of(AccountOperationLog statement) {
        return new AccountStatementResponse(statement);
    }

}
