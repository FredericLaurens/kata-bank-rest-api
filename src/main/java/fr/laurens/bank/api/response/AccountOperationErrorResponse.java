package fr.laurens.bank.api.response;

public class AccountOperationErrorResponse extends AccountOperationResponse {

    public String errorMessage;

    public AccountOperationErrorResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static AccountOperationErrorResponse of(String errorMessage) {
        return new AccountOperationErrorResponse(errorMessage);
    }
}
