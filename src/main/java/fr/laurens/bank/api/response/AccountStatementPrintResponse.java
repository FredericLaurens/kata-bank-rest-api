package fr.laurens.bank.api.response;

import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class AccountStatementPrintResponse extends AccountOperationResponse {

    public String printedDocument;

    public AccountStatementPrintResponse(String printedDocument) {
        this.printedDocument = printedDocument;
    }

    public AccountStatementPrintResponse(String errorMessage, String printedDocument) {
        super();
        this.printedDocument = printedDocument;
    }

    public static AccountStatementPrintResponse of(String printedDocument) {
        return new AccountStatementPrintResponse(printedDocument);
    }

    public static AccountStatementPrintResponse ofError(String errorMessage) {
        return new AccountStatementPrintResponse(errorMessage, null);
    }
}
