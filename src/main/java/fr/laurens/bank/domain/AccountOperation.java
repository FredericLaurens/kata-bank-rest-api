package fr.laurens.bank.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

public record AccountOperation(OperationType operationType, BigDecimal amount, LocalDate date,
                               BigDecimal resultingBalance) {
}


