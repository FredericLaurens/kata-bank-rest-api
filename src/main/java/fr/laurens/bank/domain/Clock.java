package fr.laurens.bank.domain;

import java.time.LocalDate;

public class Clock {

    private static final Clock instance = new Clock();

    private Clock() {

    }

    public static Clock getInstance() {
        return instance;
    }

    public LocalDate now() {
        return LocalDate.now();
    }
}

