package fr.laurens.bank.domain;

public enum OperationType {

    DEPOSIT("deposit"), WITHDRAWAL("withdrawal");

    private final String name;

    OperationType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
