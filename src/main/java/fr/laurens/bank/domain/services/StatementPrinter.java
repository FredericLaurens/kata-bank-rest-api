package fr.laurens.bank.domain.services;

import fr.laurens.bank.domain.AccountOperationLog;

public interface StatementPrinter {

    String printStatement(AccountOperationLog operationLog);
}
