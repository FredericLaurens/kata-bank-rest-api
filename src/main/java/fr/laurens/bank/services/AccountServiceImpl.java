package fr.laurens.bank.services;

import fr.laurens.bank.domain.Account;
import fr.laurens.bank.domain.AccountOperationLog;
import fr.laurens.bank.domain.services.StatementPrinter;
import fr.laurens.bank.persistence.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    StatementPrinter statementPrinter;

    public Account retrieveAccountByAccountID(String accountID) {
        Optional<Account> optionalAccount = accountRepository.findById(accountID);
        if (optionalAccount.isPresent()) {
            return optionalAccount.get();
        } else {
            throw new IllegalArgumentException(String.format("Account %s doesn't exist", accountID));
        }
    }

    @Override
    public void deposit(String accountID, BigDecimal amount) {
        Account account = retrieveAccountByAccountID(accountID);
        account.deposit(amount);
    }

    @Override
    public void withdraw(String accountID, BigDecimal amount) {
        retrieveAccountByAccountID(accountID).withdraw(amount);
    }

    @Override
    public AccountOperationLog generateAccountStatement(String accountID) {
        return retrieveAccountByAccountID(accountID).getAccountOperationLog();
    }

    @Override
    public String printAccountStatement(String accountID) {
        AccountOperationLog accountOperationLog = retrieveAccountByAccountID(accountID).getAccountOperationLog();
        return statementPrinter.printStatement(accountOperationLog);
    }
}
