package fr.laurens.bank.printer;

import fr.laurens.bank.domain.AccountOperation;
import fr.laurens.bank.domain.AccountOperationLog;
import fr.laurens.bank.domain.services.StatementPrinter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BasicStatementPrinter implements StatementPrinter {

    public static final String OPERATION_HEADER = "Operation";
    public static final String DATE_HEADER = "Date";
    public static final String AMOUNT_HEADER = "Amount";
    public static final String BALANCE_HEADER = "Balance";
    private static final PrintConfiguration MIN_WIDTH = new PrintConfiguration(9, 4, 6, 7);

    private static final String DATE_FORMAT = "dd/MM/YYYY";
    private static final String LINE_FORMAT = "| %s | %s | %s | %s |";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private static final BasicStatementPrinter instance = new BasicStatementPrinter();

    private final static DecimalFormat AMOUNT_FORMAT;

    static {
        AMOUNT_FORMAT = new DecimalFormat();
        AMOUNT_FORMAT.setMaximumFractionDigits(2);
        AMOUNT_FORMAT.setMinimumFractionDigits(2);
        AMOUNT_FORMAT.setCurrency(Currency.getInstance(Locale.FRANCE));
    }

    public BasicStatementPrinter() {

    }

    public static BasicStatementPrinter getInstance() {
        return instance;
    }

    private String printDate(LocalDate date) {

        return formatter.format(date);
    }

    private String printHeader(PrintConfiguration printConfiguration) {

        return String.format(LINE_FORMAT,
                StringUtils.rightPad(OPERATION_HEADER, printConfiguration.operationWidth()),
                StringUtils.rightPad(DATE_HEADER, printConfiguration.dateWidth()),
                StringUtils.rightPad(AMOUNT_HEADER, printConfiguration.amountWidth()),
                StringUtils.rightPad(BALANCE_HEADER, printConfiguration.resultingBalanceWidth()));
    }

    private String printOperation(AccountOperation accountOperation, PrintConfiguration printConfiguration) {

        return String.format(LINE_FORMAT,
                StringUtils.rightPad(accountOperation.operationType().toString(), printConfiguration.operationWidth()),
                StringUtils.rightPad(printDate(accountOperation.date()), printConfiguration.dateWidth()),
                StringUtils.rightPad(AMOUNT_FORMAT.format(accountOperation.amount()), printConfiguration.amountWidth()),
                StringUtils.rightPad(AMOUNT_FORMAT.format(accountOperation.resultingBalance()), printConfiguration.resultingBalanceWidth()));
    }

    private PrintConfiguration printConfigurationReducer(PrintConfiguration printConfiguration, AccountOperation accountOperation) {

        int operationLength = accountOperation.operationType().toString().length();
        if (operationLength < printConfiguration.operationWidth()) {
            operationLength = printConfiguration.operationWidth();
        }
        int dateLength = printDate(accountOperation.date()).length();
        if (dateLength < printConfiguration.dateWidth()) {
            dateLength = printConfiguration.dateWidth();
        }

        int amountLength = AMOUNT_FORMAT.format(accountOperation.amount()).length();
        if (amountLength < printConfiguration.amountWidth()) {
            amountLength = printConfiguration.amountWidth();
        }

        int balanceLength = AMOUNT_FORMAT.format(accountOperation.resultingBalance()).length();
        if (balanceLength < printConfiguration.resultingBalanceWidth()) {
            balanceLength = printConfiguration.resultingBalanceWidth();
        }

        return new PrintConfiguration(operationLength, dateLength, amountLength, balanceLength);
    }

    private PrintConfiguration printConfigurationCombiner(PrintConfiguration printConfiguration1, PrintConfiguration printConfiguration2) {

        return new PrintConfiguration(
                Integer.max(printConfiguration1.operationWidth(), printConfiguration2.operationWidth()),
                Integer.max(printConfiguration1.dateWidth(), printConfiguration2.dateWidth()),
                Integer.max(printConfiguration1.amountWidth(), printConfiguration2.amountWidth()),
                Integer.max(printConfiguration1.resultingBalanceWidth(), printConfiguration2.resultingBalanceWidth()));
    }

    public String printStatement(AccountOperationLog operationLog) {

        List<AccountOperation> accountOperations = new ArrayList<>(operationLog.getAccountOperations());
        Collections.reverse(accountOperations);

        PrintConfiguration printConfiguration = accountOperations.stream().reduce(MIN_WIDTH, this::printConfigurationReducer, this::printConfigurationCombiner);

        String operations = accountOperations.stream().map(operation -> printOperation(operation, printConfiguration)).collect(Collectors.joining("\n"));

        if (operations.length() == 0) {
            return printHeader(printConfiguration);
        } else {
            return printHeader(printConfiguration) + "\n" + operations + "\n";
        }
    }
}
