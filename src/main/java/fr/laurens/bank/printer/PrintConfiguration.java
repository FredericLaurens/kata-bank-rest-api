package fr.laurens.bank.printer;

public record PrintConfiguration(int operationWidth, int dateWidth, int amountWidth, int resultingBalanceWidth) {
}